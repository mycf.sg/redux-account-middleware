import {addMinutes, format} from 'date-fns';
import {ACCOUNT_RECEIVED} from '../constants';
import {IAccountState} from '../reducer';

export interface IAccountMock {
  foo: string;
  bar: boolean;
  baz: number;
  exp: number;
}

export const accountMock: IAccountMock = {
  bar: true,
  baz: 123,
  exp: new Date().getTime() / 1000,
  foo: 'string',
};

export const accountMockWithCustomExpiry = (minutesToExpiry: number): IAccountMock => {
  const tokenExpiry = addMinutes(Date.now(), minutesToExpiry);
  return {
    ...accountMock,
    exp: parseInt(format(tokenExpiry, 'X'), 10),
  };
};

export const accountStateMock: IAccountState<IAccountMock> = {
  data: accountMock,
  status: ACCOUNT_RECEIVED,
};
