import {differenceInMilliseconds, subMinutes} from 'date-fns';
import {Dispatch} from 'redux';
import {logOutReceived, refreshCancelled, refreshRequested, sessionExpired} from './actions';

export class Timer {
  private dispatch: Dispatch;
  private localStorageKey: string;
  private tokenExpiryConfirmIntervalMinutes: number;
  private tokenExpiryConfirmMessage: string;
  private timeoutID?: NodeJS.Timeout;

  constructor(
    dispatch: Dispatch,
    localStorageKey: string,
    tokenExpiryConfirmIntervalMinutes: number,
    tokenExpiryConfirmMessage: string,
  ) {
    this.dispatch = dispatch;
    this.localStorageKey = localStorageKey;
    this.tokenExpiryConfirmIntervalMinutes = tokenExpiryConfirmIntervalMinutes;
    this.tokenExpiryConfirmMessage = tokenExpiryConfirmMessage;
  }

  public clearTimer() {
    if (this.timeoutID) {
      clearTimeout(this.timeoutID);
    }
  }

  public setTimer(tokenExpiry: number) {
    const tokenExpiryInMilliseconds = tokenExpiry * 1000;
    const currentDatetime = new Date();
    const alertTime = subMinutes(tokenExpiryInMilliseconds, this.tokenExpiryConfirmIntervalMinutes);
    const millisecondsToAlert = differenceInMilliseconds(alertTime, currentDatetime);
    const millisecondsToExpire = differenceInMilliseconds(tokenExpiryInMilliseconds, currentDatetime);

    if (millisecondsToExpire > 0) {
      // determine whether to show confirm dialog to tell user the token is expiring
      // true = show confirm dialog
      // false = dispatch session expired action

      /**
       * To show confirmation dialog if it's about to expire
       */
      const showConfirm = millisecondsToAlert > 0;
      const timeoutMilliseconds = showConfirm ? millisecondsToAlert : millisecondsToExpire;

      this.clearTimer();

      this.timeoutID = setTimeout(() => {
        this.onTimeout(tokenExpiry, showConfirm);
      }, timeoutMilliseconds);
    } else {
      this.dispatch(sessionExpired());
    }
  }

  private onTimeout(tokenExpiry: number, showConfirm: boolean) {
    // 1. If there's no token in localStorage
    // @example
    // User logged out in another tab or window
    const tokenFromLocalStorage = Number(localStorage.getItem(this.localStorageKey));
    if (!tokenFromLocalStorage) {
      this.dispatch(logOutReceived());
      return;
    }

    // 2. If token exists and greater then token before timeout
    // @example
    // User refreshes the token in another tab or window
    if (tokenFromLocalStorage > tokenExpiry) {
      this.setTimer(tokenFromLocalStorage);
      return;
    }

    // 3. If token is about to expire show confirmation dialog
    if (showConfirm) {
      if (document.hidden) {
        // don't show the confirm dialog for the background tab/window
        // set timer to dispatch session expired action
        this.setTimer(tokenFromLocalStorage);
        return;
      }

      if (confirm(this.tokenExpiryConfirmMessage)) {
        // if user clicks ok, refresh the token
        // but don't set the timer here because
        // is in the middle of getting new token
        this.dispatch(refreshRequested());
      } else {
        // if user clicks cancel, dispatch refresh cancelled action
        // set timer to dispatch session expired action
        this.dispatch(refreshCancelled());
        this.setTimer(tokenFromLocalStorage);
      }
    } else {
      this.dispatch(sessionExpired());
    }
  }
}
