export const ACCOUNT_REQUESTED = '@@account/ACCOUNT_REQUESTED';
export const ACCOUNT_RECEIVED = '@@account/ACCOUNT_RECEIVED';
export const ACCOUNT_REQUEST_FAILED = '@@account/ACCOUNT_REQUEST_FAILED';

export const REFRESH_REQUESTED = '@@account/REFRESH_REQUESTED';
export const REFRESH_RECEIVED = '@@account/REFRESH_RECEIVED';
export const REFRESH_REQUEST_FAILED = '@@account/REFRESH_REQUEST_FAILED';
export const REFRESH_CANCELLED = '@@account/REFRESH_CANCELLED';

export const LOGOUT_REQUESTED = '@@account/LOGOUT_REQUESTED';
export const LOGOUT_RECEIVED = '@@account/LOGOUT_RECEIVED';
export const LOGOUT_REQUEST_FAILED = '@@account/LOGOUT_REQUEST_FAILED';

export const SESSION_EXPIRED = '@@account/SESSION_EXPIRED';
