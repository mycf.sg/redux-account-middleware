import {ActionType} from 'typesafe-actions';
import * as actions from './actions';
import * as constants from './constants';

export const accountReducer = <T extends IAccountData>(
  state: IAccountState<T> = {},
  action: AccountActionType<T>,
): IAccountState<T> => {
  switch (action.type) {
    case constants.ACCOUNT_RECEIVED:
      return {
        data: action.payload,
        status: action.type,
      };
    case constants.LOGOUT_RECEIVED:
    case constants.SESSION_EXPIRED:
      return {
        status: action.type,
      };
    case constants.ACCOUNT_REQUESTED:
    case constants.ACCOUNT_REQUEST_FAILED:
    case constants.REFRESH_RECEIVED:
    case constants.REFRESH_REQUESTED:
    case constants.REFRESH_REQUEST_FAILED:
    case constants.REFRESH_CANCELLED:
    case constants.LOGOUT_REQUESTED:
    case constants.LOGOUT_REQUEST_FAILED:
      return {
        ...state,
        status: action.type,
      };
    default:
      return state;
  }
};

export interface IAccountState<T extends IAccountData> {
  data?: T;
  status?: AccountConstantType;
}

export interface IAccountData {
  exp: number;
}

// this type will restrict the allowed values to values from constants only
export type AccountConstantType = typeof constants[keyof typeof constants];

// accountReceived action has a generic type payload which will make AccountActionType to return any type
// so we exclude accountReceived from ActionType helper and construct its type manually
export const {accountReceived: _, ...restActions} = actions;
export type AccountActionType<T extends IAccountData> =
  | ActionType<typeof restActions>
  | {type: typeof constants.ACCOUNT_RECEIVED; payload: T};
