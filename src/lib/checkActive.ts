import {Dispatch} from 'redux';
import {IAccountData, IAccountState} from './reducer';

import {accountRequested, logOutReceived, refreshRequested, sessionExpired} from './actions';

import {differenceInMilliseconds, differenceInMinutes} from 'date-fns';

import {ACCOUNT_REQUESTED, LOGOUT_REQUESTED, REFRESH_RECEIVED, REFRESH_REQUESTED} from './constants';

interface ICheckActive {
  accountReducerKey: string;
  dispatch: Dispatch;
  getState: () => any;
  localStorageKey: string;
  refreshIntervalMinutes: number;
}

export const checkActive = <T extends IAccountData>({
  accountReducerKey,
  dispatch,
  getState,
  localStorageKey,
  refreshIntervalMinutes,
}: ICheckActive) => {
  const state: IAccountState<T> = getState()[accountReducerKey] || {};
  if (state.data) {
    const tokenExpiry = Number(localStorage.getItem(localStorageKey));

    if (tokenExpiry) {
      const tokenExpiryInMilliseconds = tokenExpiry * 1000;
      const currentDatetime = new Date();
      const millisecondsToExpire = differenceInMilliseconds(tokenExpiryInMilliseconds, currentDatetime);
      const minutesToExpire = differenceInMinutes(tokenExpiryInMilliseconds, currentDatetime);

      const tokenHasExpired = millisecondsToExpire <= 0;

      if (tokenHasExpired) {
        dispatch(sessionExpired());
      } else if (minutesToExpire < refreshIntervalMinutes) {
        /**
         * We should not refresh the token when the account is of the following status:
         * - REFRESH_REQUESTED  : When refresh is already requested and in progress
         *                        This is to prevent duplicates refresh request
         * - REFRESH_RECEIVED   : This is the intermediate state where account api will be called in next action
         *                        This is to prevent duplicates refresh request
         * - ACCOUNT_REQUESTED  : This is the intermediate state to get the latest token expiry from account api
         *                        This is to prevent duplicates refresh request
         * - LOGOUT_REQUESTED   : When there is a logout request on-going, We should stop refreshing the token
         *                        This is so that the user session will not be re-populated with the fresh token and log the user back in
         */
        const accountStatusThatShouldNotRefreshToken = [
          REFRESH_REQUESTED,
          REFRESH_RECEIVED,
          ACCOUNT_REQUESTED,
          LOGOUT_REQUESTED,
        ];

        const status = state.status || '';

        if (!accountStatusThatShouldNotRefreshToken.includes(status)) {
          dispatch(refreshRequested());
        }
      } else if (tokenExpiry !== state.data.exp) {
        dispatch(accountRequested());
      }
    } else {
      dispatch(logOutReceived());
    }
  }
};
