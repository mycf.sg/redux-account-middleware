import {differenceInMinutes} from 'date-fns';
import {AnyAction, Dispatch, Middleware, MiddlewareAPI} from 'redux';
import {
  accountReceived,
  accountRequested,
  accountRequestFailed,
  logOutReceived,
  logOutRequestFailed,
  refreshReceived,
  refreshRequested,
  refreshRequestFailed,
  sessionExpired,
} from './actions';
import {
  ACCOUNT_RECEIVED,
  ACCOUNT_REQUESTED,
  LOGOUT_RECEIVED,
  LOGOUT_REQUESTED,
  REFRESH_CANCELLED,
  REFRESH_RECEIVED,
  REFRESH_REQUEST_FAILED,
  REFRESH_REQUESTED,
  SESSION_EXPIRED,
} from './constants';
import {IAccountData} from './reducer';

import {checkActive} from './checkActive';

import {Timer} from './Timer';

const REFRESH_CONFIRM_MESSAGE = `You will soon be logged out due to inactivity. \
\n\nClick OK to stay logged in or Cancel to log out.`;

export const createAccountMiddleware = <T extends IAccountData>({
  accountReducerKey = 'account',
  localStorageKey = 'account-expiry',
  refreshIntervalMinutes = 15,
  tokenExpiryConfirmIntervalMinutes = 3,
  tokenExpiryConfirmMessage = REFRESH_CONFIRM_MESSAGE,
  fetchAccountRequest,
  refreshTokenRequest,
  logoutRequest,
}: {
  accountReducerKey?: string;
  localStorageKey?: string;
  refreshIntervalMinutes?: number;
  tokenExpiryConfirmIntervalMinutes?: number;
  tokenExpiryConfirmMessage?: string;
  fetchAccountRequest: () => Promise<T>;
  refreshTokenRequest: () => Promise<any>;
  logoutRequest: () => Promise<any>;
}): Middleware => {
  /**
   * +------------------------+--------------------------------------------------------------+
   * | Token Expiry time left | actions                                                      |
   * +------------------------+--------------------------------------------------------------|
   * | 0 min < X < 15 mins    | Refresh token when there is activity.                        |
   * |(refreshIntervalMinutes)| dispatch REFRESH_REQUESTED action                            |
   * |------------------------|--------------------------------------------------------------|
   * | X = 3 mins (tokenExpiry| Prompt inactive user that token is expiring soon. This prompt|
   * | ConfirmIntervalMinutes)| timer is recalculated on every ACCOUNT_RECEIVED action       |
   * |                        | (e.g. initial fetch account, refresh token)                  |
   * |                        | When user click OK, dispatch REFRESH_REQUESTED action.       |
   * |                        | When user click CANCEL, dispatch REFRESH_CANCELLED action    |
   * |------------------------|--------------------------------------------------------------|
   * | X <= 0 min             | Token expired. dispatch SESSION_EXPIRED action               |
   * |------------------------|--------------------------------------------------------------|
   */
  return ({dispatch, getState}: MiddlewareAPI) => {
    const timer = new Timer(dispatch, localStorageKey, tokenExpiryConfirmIntervalMinutes, tokenExpiryConfirmMessage);

    return (next: Dispatch) => (action: AnyAction) => {
      let shouldCheckActive = false;
      let actionToBeDispatchedLater: AnyAction | null = null;

      switch (action.type) {
        case ACCOUNT_REQUESTED:
          fetchAccountRequest()
            .then((payload) => dispatch(accountReceived<T>(payload)))
            .catch((error) => dispatch(accountRequestFailed(error)));
          break;
        case ACCOUNT_RECEIVED:
          const payloadTokenExpiry = action.payload.exp;
          if (payloadTokenExpiry) {
            localStorage.setItem(localStorageKey, payloadTokenExpiry);
            timer.setTimer(payloadTokenExpiry);
            const minutesToExpire = differenceInMinutes(payloadTokenExpiry * 1000, new Date());
            if (minutesToExpire < refreshIntervalMinutes) {
              actionToBeDispatchedLater = refreshRequested();
            }
          }
          break;
        case REFRESH_REQUESTED:
          refreshTokenRequest()
            .then(() => dispatch(refreshReceived()))
            .catch((error) => {
              dispatch(refreshRequestFailed(error));
            });
          break;
        case REFRESH_RECEIVED:
          actionToBeDispatchedLater = accountRequested();
          break;
        case REFRESH_REQUEST_FAILED:
          const tokenFromLocalStorage = localStorage.getItem(localStorageKey);
          const tokenExpiry = Number(tokenFromLocalStorage);
          if (!tokenExpiry) {
            actionToBeDispatchedLater = sessionExpired();
          }
          break;
        case LOGOUT_REQUESTED:
          logoutRequest()
            .then(() => dispatch(logOutReceived()))
            .catch((error) => dispatch(logOutRequestFailed(error)));
          break;
        case LOGOUT_RECEIVED:
        case SESSION_EXPIRED:
          localStorage.removeItem(localStorageKey);
          timer.clearTimer();
          break;
        case REFRESH_CANCELLED:
          break;
        default:
          shouldCheckActive = true;
      }

      const result = next(action);

      // checkActive should be called after next(action)
      // 1. to dispatch the action in the correct order
      // 2. in the event of REFRESH_REQUEST_FAILED, checkActive will retry refresh token api again
      if (shouldCheckActive) {
        checkActive<T>({accountReducerKey, dispatch, getState, localStorageKey, refreshIntervalMinutes});
      }

      if (actionToBeDispatchedLater !== null) {
        dispatch(actionToBeDispatchedLater);
      }

      return result;
    };
  };
};
