import {action} from 'typesafe-actions';
import {
  ACCOUNT_RECEIVED,
  ACCOUNT_REQUEST_FAILED,
  ACCOUNT_REQUESTED,
  LOGOUT_RECEIVED,
  LOGOUT_REQUEST_FAILED,
  LOGOUT_REQUESTED,
  REFRESH_CANCELLED,
  REFRESH_RECEIVED,
  REFRESH_REQUEST_FAILED,
  REFRESH_REQUESTED,
  SESSION_EXPIRED,
} from './constants';
import {IAccountData} from './reducer';

export const accountRequested = () => action(ACCOUNT_REQUESTED);
export const accountReceived = <T extends IAccountData>(account: T) => action(ACCOUNT_RECEIVED, account);
export const accountRequestFailed = (error: Error) => action(ACCOUNT_REQUEST_FAILED, error);

export const refreshRequested = () => action(REFRESH_REQUESTED);
export const refreshReceived = () => action(REFRESH_RECEIVED);
export const refreshRequestFailed = (error: Error) => action(REFRESH_REQUEST_FAILED, error);
export const refreshCancelled = () => action(REFRESH_CANCELLED);

export const logOutRequested = () => action(LOGOUT_REQUESTED);
export const logOutReceived = () => action(LOGOUT_RECEIVED);
export const logOutRequestFailed = (error: Error) => action(LOGOUT_REQUEST_FAILED, error);

export const sessionExpired = () => action(SESSION_EXPIRED);
