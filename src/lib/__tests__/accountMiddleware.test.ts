import configureStore from 'redux-mock-store';
import {action} from 'typesafe-actions';
import {accountMock, accountMockWithCustomExpiry} from '../__mocks__/account.mock';
import {
  accountReceived,
  accountRequested,
  accountRequestFailed,
  logOutReceived,
  logOutRequested,
  logOutRequestFailed,
  refreshReceived,
  refreshRequested,
  refreshRequestFailed,
  sessionExpired,
  refreshCancelled,
} from '../actions';

import * as createAccountMiddleware from '../createAccountMiddleware';

const fetchAccountRequest = jest.fn();
const refreshTokenRequest = jest.fn();
const logoutRequest = jest.fn();

const accountReducerKey = 'test';
const localStorageKey = 'test-expiry';
const refreshIntervalMinutes = 5;
const tokenExpiryConfirmIntervalMinutes = 1;
const tokenExpiryConfirmMessage = 'foo bar';
const accountMiddleware = createAccountMiddleware.createAccountMiddleware({
  accountReducerKey,
  fetchAccountRequest,
  localStorageKey,
  logoutRequest,
  refreshIntervalMinutes,
  refreshTokenRequest,
  tokenExpiryConfirmIntervalMinutes,
  tokenExpiryConfirmMessage,
});
const promiseRejectionError = new Error('error');

const mockStore = configureStore([accountMiddleware]);
const store = mockStore({});

window.confirm = jest.fn();

describe('accountMiddleware', () => {
  beforeEach(() => {
    fetchAccountRequest.mockResolvedValue(accountMock);
    refreshTokenRequest.mockReturnValue(Promise.resolve());
    logoutRequest.mockReturnValue(Promise.resolve());

    store.clearActions();
  });

  afterEach(() => {
    fetchAccountRequest.mockReset();
    refreshTokenRequest.mockReset();
    logoutRequest.mockReset();
  });

  describe('LOGOUT_RECEIVED', () => {
    beforeEach(() => {
      jest.useFakeTimers();
    });

    afterEach(() => {
      jest.useRealTimers();
      jest.restoreAllMocks();
    });

    it('should not display refresh token confirm message anymore when logged out', () => {
      const twoMinutes = tokenExpiryConfirmIntervalMinutes + 1;
      const accountWithExpiryBeyondConfirmInterval = accountMockWithCustomExpiry(twoMinutes);
      store.dispatch(accountReceived(accountWithExpiryBeyondConfirmInterval));

      store.dispatch(logOutReceived());
      jest.runAllTimers();
      expect(confirm).not.toHaveBeenCalled();
    });
  });
});
