import {accountMock, IAccountMock} from '../__mocks__/account.mock';
import {
  accountReceived,
  accountRequested,
  accountRequestFailed,
  logOutReceived,
  logOutRequested,
  logOutRequestFailed,
  refreshCancelled,
  refreshReceived,
  refreshRequested,
  refreshRequestFailed,
  sessionExpired,
} from '../actions';
import {
  ACCOUNT_RECEIVED,
  ACCOUNT_REQUEST_FAILED,
  ACCOUNT_REQUESTED,
  LOGOUT_RECEIVED,
  LOGOUT_REQUEST_FAILED,
  LOGOUT_REQUESTED,
  REFRESH_CANCELLED,
  REFRESH_RECEIVED,
  REFRESH_REQUEST_FAILED,
  REFRESH_REQUESTED,
  SESSION_EXPIRED,
} from '../constants';
import {accountReducer} from '../reducer';

describe('account reducer', () => {
  const initialAccount: IAccountMock = {foo: 'bar', bar: true, baz: 9, exp: 123};
  const initialState = {data: initialAccount};

  describe('ACCOUNT_REQUESTED', () => {
    it('should set status to ACCOUNT_REQUESTED', () => {
      expect(accountReducer(initialState, accountRequested())).toEqual({
        ...initialState,
        status: ACCOUNT_REQUESTED,
      });
    });
  });

  describe('ACCOUNT_RECEIVED', () => {
    it('should set status to ACCOUNT_RECEIVED and set data/exp based on accounts data', () => {
      expect(accountReducer(initialState, accountReceived(accountMock))).toEqual({
        data: accountMock,
        status: ACCOUNT_RECEIVED,
      });
    });
  });

  describe('REFRESH_REQUEST_FAILED', () => {
    it('should set status to ACCOUNT_REQUEST_FAILED', () => {
      expect(accountReducer(initialState, accountRequestFailed(new Error()))).toEqual({
        ...initialState,
        status: ACCOUNT_REQUEST_FAILED,
      });
    });
  });

  describe('REFRESH_REQUESTED', () => {
    it('should set status to REFRESH_REQUESTED', () => {
      expect(accountReducer(initialState, refreshRequested())).toEqual({
        ...initialState,
        status: REFRESH_REQUESTED,
      });
    });
  });

  describe('REFRESH_RECEIVED', () => {
    it('should set status to REFRESH_RECEIVED', () => {
      expect(accountReducer(initialState, refreshReceived())).toEqual({
        ...initialState,
        status: REFRESH_RECEIVED,
      });
    });
  });

  describe('REFRESH_REQUEST_FAILED', () => {
    it('should set status to REFRESH_REQUEST_FAILED', () => {
      expect(accountReducer(initialState, refreshRequestFailed(new Error()))).toEqual({
        ...initialState,
        status: REFRESH_REQUEST_FAILED,
      });
    });
  });

  describe('REFRESH_CANCELLED', () => {
    it('should set status to REFRESH_CANCELLED', () => {
      expect(accountReducer(initialState, refreshCancelled())).toEqual({
        ...initialState,
        status: REFRESH_CANCELLED,
      });
    });
  });

  describe('LOGOUT_REQUESTED', () => {
    it('should set status to LOGOUT_REQUESTED', () => {
      expect(accountReducer(initialState, logOutRequested())).toEqual({
        ...initialState,
        status: LOGOUT_REQUESTED,
      });
    });
  });

  describe('LOGOUT_RECEIVED', () => {
    it('should clear the account state and set status to LOGOUT_RECEIVED', () => {
      expect(accountReducer(initialState, logOutReceived())).toEqual({
        status: LOGOUT_RECEIVED,
      });
    });
  });

  describe('LOGOUT_REQUEST_FAILED', () => {
    it('should set status to LOGOUT_REQUEST_FAILED', () => {
      expect(accountReducer(initialState, logOutRequestFailed(new Error()))).toEqual({
        ...initialState,
        status: LOGOUT_REQUEST_FAILED,
      });
    });
  });

  describe('SESSION_EXPIRED', () => {
    it('should clear the account state and set status to SESSION_EXPIRED', () => {
      expect(accountReducer(initialState, sessionExpired())).toEqual({
        status: SESSION_EXPIRED,
      });
    });
  });
});
