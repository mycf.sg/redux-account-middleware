import {accountMockWithCustomExpiry} from '../__mocks__/account.mock';
import {logOutReceived, refreshCancelled, refreshRequested, sessionExpired} from '../actions';
import {Timer} from '../Timer';

const localStorageKey = 'test-expiry';
const tokenExpiryConfirmIntervalMinutes = 1;
const tokenExpiryConfirmMessage = 'foo bar';

window.confirm = jest.fn();

describe('Timer', () => {
  let setTimerSpy: jest.SpyInstance;
  let clearTimerSpy: jest.SpyInstance;

  beforeEach(() => {
    jest.resetModules();

    jest.useFakeTimers();
    localStorage.clear();

    setTimerSpy = jest.spyOn(Timer.prototype, 'setTimer');
    clearTimerSpy = jest.spyOn(Timer.prototype, 'clearTimer');
  });

  afterEach(() => {
    jest.useRealTimers();
    jest.restoreAllMocks();
  });

  describe('token is expired', () => {
    const account = accountMockWithCustomExpiry(-1);
    const tokenExpiry = account.exp;

    it('should not clear and set timer, then dispatch SESSION_EXPIRED', () => {
      localStorage.setItem(localStorageKey, String(tokenExpiry));
      const mockDispatch = jest.fn();
      const timer = new Timer(
        mockDispatch,
        localStorageKey,
        tokenExpiryConfirmIntervalMinutes,
        tokenExpiryConfirmMessage,
      );
      timer.setTimer(tokenExpiry);

      expect(clearTimerSpy).not.toHaveBeenCalledTimes(1);
      expect(setTimeout).not.toHaveBeenCalledTimes(1);
      expect(mockDispatch).toHaveBeenNthCalledWith(1, sessionExpired());
    });
  });

  describe('token expiry is beyond the tokenExpiryConfirmIntervalMinutes (useAlertFlow = true)', () => {
    const minsLeft = tokenExpiryConfirmIntervalMinutes + 1;
    const account = accountMockWithCustomExpiry(minsLeft);
    const tokenExpiry = account.exp;

    it('should clear and set timer, then check the new token from local storage', () => {
      localStorage.setItem(localStorageKey, String(tokenExpiry));
      const timer = new Timer(jest.fn(), localStorageKey, tokenExpiryConfirmIntervalMinutes, tokenExpiryConfirmMessage);
      timer.setTimer(tokenExpiry);
      expect(clearTimerSpy).toHaveBeenCalledTimes(1);
      expect(setTimeout).toHaveBeenCalledTimes(1);

      const localStorageGetItemSpy = jest.spyOn(Storage.prototype, 'getItem');
      jest.runOnlyPendingTimers();
      expect(localStorageGetItemSpy).toBeCalledWith(localStorageKey);
    });

    it('should dispatch LOGOUT_RECEIVED if new token does not exist', () => {
      const dispatchMock = jest.fn();
      const timer = new Timer(
        dispatchMock,
        localStorageKey,
        tokenExpiryConfirmIntervalMinutes,
        tokenExpiryConfirmMessage,
      );
      timer.setTimer(tokenExpiry);
      expect(clearTimerSpy).toHaveBeenCalledTimes(1);
      expect(setTimeout).toHaveBeenCalledTimes(1);

      setTimerSpy.mockReset();

      const localStorageGetItemSpy = jest.spyOn(Storage.prototype, 'getItem');
      jest.runOnlyPendingTimers();
      expect(localStorageGetItemSpy).toBeCalledWith(localStorageKey);

      expect(dispatchMock).toHaveBeenNthCalledWith(1, logOutReceived());
      expect(setTimerSpy).not.toHaveBeenCalled();
    });

    it('should call setTimer if new token expiry is bigger than old one', () => {
      const dispatchMock = jest.fn();
      const timer = new Timer(
        dispatchMock,
        localStorageKey,
        tokenExpiryConfirmIntervalMinutes,
        tokenExpiryConfirmMessage,
      );
      timer.setTimer(tokenExpiry);

      setTimerSpy.mockReset();

      localStorage.setItem(localStorageKey, String(Number.MAX_SAFE_INTEGER));

      jest.runOnlyPendingTimers();

      expect(setTimerSpy).toHaveBeenCalledTimes(1);
      expect(dispatchMock).not.toHaveBeenCalled();
    });
  });

  describe('new token expiry is same or smaller than old one', () => {
    const minsLeft = tokenExpiryConfirmIntervalMinutes + 1;

    const account = accountMockWithCustomExpiry(minsLeft);
    const tokenExpiry = account.exp;

    it('should setTimer if isDocumentHidden = true', () => {
      // NOTE: https://github.com/jsdom/jsdom/issues/2391#issuecomment-429085358
      Object.defineProperty(document, 'hidden', {
        configurable: true,
        get() {
          return true;
        },
      });

      const dispatchMock = jest.fn();
      localStorage.setItem(localStorageKey, String(tokenExpiry));

      const timer = new Timer(
        dispatchMock,
        localStorageKey,
        tokenExpiryConfirmIntervalMinutes,
        tokenExpiryConfirmMessage,
      );
      timer.setTimer(tokenExpiry);

      setTimerSpy.mockReset();

      jest.runOnlyPendingTimers();
      expect(dispatchMock).not.toHaveBeenCalled();
    });

    it('should display a confirm with message if isDocumentHidden = false', () => {
      // NOTE: https://github.com/jsdom/jsdom/issues/2391#issuecomment-429085358
      Object.defineProperty(document, 'hidden', {
        configurable: true,
        get() {
          return false;
        },
      });

      const dispatchMock = jest.fn();
      localStorage.setItem(localStorageKey, String(tokenExpiry));

      const timer = new Timer(
        dispatchMock,
        localStorageKey,
        tokenExpiryConfirmIntervalMinutes,
        tokenExpiryConfirmMessage,
      );
      timer.setTimer(tokenExpiry);

      setTimerSpy.mockReset();

      jest.runOnlyPendingTimers();
      expect(dispatchMock).toHaveBeenCalled();
      expect(confirm).toHaveBeenNthCalledWith(1, tokenExpiryConfirmMessage);
    });

    it('should dispatch REFRESH_REQUESTED if reply is OK if isDocumentHidden = false', () => {
      // NOTE: https://github.com/jsdom/jsdom/issues/2391#issuecomment-429085358
      Object.defineProperty(document, 'hidden', {
        configurable: true,
        get() {
          return false;
        },
      });

      window.confirm = jest.fn().mockReturnValue(true);

      const dispatchMock = jest.fn();
      localStorage.setItem(localStorageKey, String(tokenExpiry));

      const timer = new Timer(
        dispatchMock,
        localStorageKey,
        tokenExpiryConfirmIntervalMinutes,
        tokenExpiryConfirmMessage,
      );
      timer.setTimer(tokenExpiry);

      setTimerSpy.mockReset();

      jest.runOnlyPendingTimers();
      expect(confirm).toHaveBeenNthCalledWith(1, tokenExpiryConfirmMessage);
      expect(dispatchMock).toHaveBeenNthCalledWith(1, refreshRequested());
    });

    it('should dispatch REFRESH_CANCELLED and setTimer if reply is CANCEL if isDocumentHidden = false', () => {
      // NOTE: https://github.com/jsdom/jsdom/issues/2391#issuecomment-429085358
      Object.defineProperty(document, 'hidden', {
        configurable: true,
        get() {
          return false;
        },
      });

      window.confirm = jest.fn().mockReturnValue(false);

      const dispatchMock = jest.fn();
      localStorage.setItem(localStorageKey, String(tokenExpiry));

      const timer = new Timer(
        dispatchMock,
        localStorageKey,
        tokenExpiryConfirmIntervalMinutes,
        tokenExpiryConfirmMessage,
      );
      timer.setTimer(tokenExpiry);

      setTimerSpy.mockReset();

      jest.runOnlyPendingTimers();
      expect(confirm).toHaveBeenNthCalledWith(1, tokenExpiryConfirmMessage);
      expect(dispatchMock).toHaveBeenNthCalledWith(1, refreshCancelled());
      expect(setTimerSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('token expiry is within the tokenExpiryConfirmIntervalMinutes but beyond expired (useAlertFlow = false)', () => {
    const minsLeft = tokenExpiryConfirmIntervalMinutes - 0.5;

    const account = accountMockWithCustomExpiry(minsLeft);
    const tokenExpiry = account.exp;

    it('should dispatch LOGOUT_RECEIVED if new token does not exist', () => {
      const dispatchMock = jest.fn();

      const timer = new Timer(
        dispatchMock,
        localStorageKey,
        tokenExpiryConfirmIntervalMinutes,
        tokenExpiryConfirmMessage,
      );
      timer.setTimer(tokenExpiry);

      setTimerSpy.mockReset();

      jest.runOnlyPendingTimers();

      expect(dispatchMock).toHaveBeenNthCalledWith(1, logOutReceived());
      expect(setTimerSpy).not.toHaveBeenCalled();
    });

    it('should call setAlertTimer if new token expiry is bigger than old one', () => {
      const dispatchMock = jest.fn();
      localStorage.setItem(localStorageKey, String(Number.MAX_SAFE_INTEGER));

      const timer = new Timer(
        dispatchMock,
        localStorageKey,
        tokenExpiryConfirmIntervalMinutes,
        tokenExpiryConfirmMessage,
      );
      timer.setTimer(tokenExpiry);

      setTimerSpy.mockReset();

      jest.runOnlyPendingTimers();

      expect(setTimerSpy).toHaveBeenCalledTimes(1);
      expect(dispatchMock).not.toHaveBeenCalled();
    });

    it('should dispatch SESSION_EXPIRED if new token expiry is same or smaller than old one', () => {
      const dispatchMock = jest.fn();
      localStorage.setItem(localStorageKey, String(tokenExpiry));

      const timer = new Timer(
        dispatchMock,
        localStorageKey,
        tokenExpiryConfirmIntervalMinutes,
        tokenExpiryConfirmMessage,
      );
      timer.setTimer(tokenExpiry);

      setTimerSpy.mockReset();

      jest.runOnlyPendingTimers();

      expect(setTimerSpy).not.toHaveBeenCalled();
      expect(dispatchMock).toHaveBeenNthCalledWith(1, sessionExpired());
    });
  });
});
