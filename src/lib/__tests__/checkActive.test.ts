import {accountMockWithCustomExpiry, accountStateMock} from '../__mocks__/account.mock';
import {accountRequested, sessionExpired, refreshRequested, logOutReceived} from '../actions';
import {ACCOUNT_REQUESTED, LOGOUT_REQUESTED, REFRESH_RECEIVED, REFRESH_REQUESTED} from '../constants';

import {checkActive} from './../checkActive';
const accountReducerKey = 'test';
const localStorageKey = 'test-expiry';
const refreshIntervalMinutes = 5;

describe('checkActive', () => {
  beforeEach(() => {
    localStorage.clear();
  });

  const testShouldNotDispatchRefreshRequest = (status: string) => {
    const accountReadyForRefresh = accountMockWithCustomExpiry(refreshIntervalMinutes);
    const getState = () => ({[accountReducerKey]: {...accountStateMock, data: accountReadyForRefresh, status}});

    localStorage.setItem(localStorageKey, String(accountReadyForRefresh.exp));
    const mockDispatch = jest.fn();
    checkActive({
      accountReducerKey,
      dispatch: mockDispatch,
      getState,
      localStorageKey,
      refreshIntervalMinutes,
    });
    expect(mockDispatch).not.toHaveBeenCalled();
  };

  it('should dispatch REFRESH_REQUESTED when the minutes to expiry of the token < refreshIntervalMinutes', () => {
    const accountReadyForRefresh = accountMockWithCustomExpiry(refreshIntervalMinutes);
    const getState = () => ({[accountReducerKey]: {...accountStateMock, data: accountReadyForRefresh}});
    localStorage.setItem(localStorageKey, String(accountReadyForRefresh.exp));

    const dispatchMock = jest.fn();
    checkActive({
      accountReducerKey,
      dispatch: dispatchMock,
      getState,
      localStorageKey,
      refreshIntervalMinutes,
    });

    expect(dispatchMock).toHaveBeenNthCalledWith(1, refreshRequested());
  });

  it('should not dispatch REFRESH_REQUESTED when the minutes to expiry of the token > refreshIntervalMinutes', () => {
    const accountBeyondRefresh = accountMockWithCustomExpiry(refreshIntervalMinutes + 1);
    const getState = () => ({[accountReducerKey]: {...accountStateMock, data: accountBeyondRefresh}});
    localStorage.setItem(localStorageKey, String(accountBeyondRefresh.exp));

    const dispatchMock = jest.fn();

    checkActive({
      accountReducerKey,
      dispatch: dispatchMock,
      getState,
      localStorageKey,
      refreshIntervalMinutes,
    });

    expect(dispatchMock).not.toHaveBeenCalled();
  });

  it('should not dispatch REFRESH_REQUESTED when status = REFRESH_REQUESTED', () => {
    testShouldNotDispatchRefreshRequest(REFRESH_REQUESTED);
  });

  it('should not dispatch REFRESH_REQUESTED when status = REFRESH_RECEIVED', () => {
    testShouldNotDispatchRefreshRequest(REFRESH_RECEIVED);
  });

  it('should not dispatch REFRESH_REQUESTED when status = ACCOUNT_REQUESTED', () => {
    testShouldNotDispatchRefreshRequest(ACCOUNT_REQUESTED);
  });

  it('should not dispatch REFRESH_REQUESTED when status = LOGOUT_REQUESTED', () => {
    testShouldNotDispatchRefreshRequest(LOGOUT_REQUESTED);
  });

  it('should dispatch LOGOUT_RECEIVED if theres no token in localStorage', () => {
    const accountReadyForRefresh = accountMockWithCustomExpiry(refreshIntervalMinutes);

    const getState = () => ({[accountReducerKey]: {...accountStateMock, data: accountReadyForRefresh}});

    const dispatchMock = jest.fn();
    checkActive({
      accountReducerKey,
      dispatch: dispatchMock,
      getState,
      localStorageKey,
      refreshIntervalMinutes,
    });

    expect(dispatchMock).toHaveBeenNthCalledWith(1, logOutReceived());
  });

  it('should dispatch SESSION_EXPIRED when token has expired', () => {
    const accountExpired = accountMockWithCustomExpiry(-1);
    const getState = () => ({[accountReducerKey]: {...accountStateMock, data: accountExpired}});

    localStorage.setItem(localStorageKey, String(accountExpired.exp));
    const mockDispatch = jest.fn();
    checkActive({
      accountReducerKey,
      dispatch: mockDispatch,
      getState,
      localStorageKey,
      refreshIntervalMinutes,
    });
    expect(mockDispatch).toHaveBeenNthCalledWith(1, sessionExpired());
  });

  it('should dispatch ACCOUNT_REQUESTED to update redux account when tokenExpiry between localStorage & redux not equal', () => {
    const accountBeyondRefresh = accountMockWithCustomExpiry(refreshIntervalMinutes + 1);

    localStorage.setItem(localStorageKey, String(accountBeyondRefresh.exp + 10));

    const getState = () => ({[accountReducerKey]: {...accountStateMock, data: accountBeyondRefresh}});

    const mockDispatch = jest.fn();
    checkActive({
      accountReducerKey,
      dispatch: mockDispatch,
      getState,
      localStorageKey,
      refreshIntervalMinutes,
    });
    expect(mockDispatch).toHaveBeenNthCalledWith(1, accountRequested());
  });
});
