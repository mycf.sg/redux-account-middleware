import configureStore from 'redux-mock-store';
import {action} from 'typesafe-actions';
import {accountMock, accountMockWithCustomExpiry} from '../__mocks__/account.mock';
import {
  accountReceived,
  accountRequested,
  accountRequestFailed,
  logOutReceived,
  logOutRequested,
  logOutRequestFailed,
  refreshCancelled,
  refreshReceived,
  refreshRequested,
  refreshRequestFailed,
  sessionExpired,
} from '../actions';

import * as deps from '../checkActive';
import * as createAccountMiddleware from '../createAccountMiddleware';
import {Timer} from '../Timer';

jest.mock('../checkActive', () => {
  return {checkActive: jest.fn()};
});

window.confirm = jest.fn();

const fetchAccountRequest = jest.fn();
const refreshTokenRequest = jest.fn();
const logoutRequest = jest.fn();

const accountReducerKey = 'test';
const localStorageKey = 'test-expiry';
const refreshIntervalMinutes = 5;
const tokenExpiryConfirmIntervalMinutes = 1;
const tokenExpiryConfirmMessage = 'foo bar';
const accountMiddleware = createAccountMiddleware.createAccountMiddleware({
  accountReducerKey,
  fetchAccountRequest,
  localStorageKey,
  logoutRequest,
  refreshIntervalMinutes,
  refreshTokenRequest,
  tokenExpiryConfirmIntervalMinutes,
  tokenExpiryConfirmMessage,
});
const promiseRejectionError = new Error('error');

const mockStore = configureStore([accountMiddleware]);
const store = mockStore({});

describe('createAccountMiddleware', () => {
  beforeEach(() => {
    fetchAccountRequest.mockResolvedValue(accountMock);
    refreshTokenRequest.mockReturnValue(Promise.resolve());
    logoutRequest.mockReturnValue(Promise.resolve());

    store.clearActions();
  });

  afterEach(() => {
    fetchAccountRequest.mockReset();
    refreshTokenRequest.mockReset();
    logoutRequest.mockReset();
  });

  describe('ACCOUNT_REQUESTED', () => {
    it('should call fetchAccountRequest then dispatch ACCOUNT_RECEIVED when fetchAccount Request succeed', async () => {
      await store.dispatch(accountRequested());
      expect(fetchAccountRequest).toBeCalled();
      const actions = store.getActions();
      expect(actions).toEqual(expect.arrayContaining([accountRequested(), accountReceived(accountMock)]));
    });

    it('should call fetchAccountRequest then dispatch ACCOUNT_REQUEST_FAILED when fetchAccount Request fail', async () => {
      fetchAccountRequest.mockRejectedValue(promiseRejectionError);
      await store.dispatch(accountRequested());
      expect(fetchAccountRequest).toBeCalled();
      const actions = await store.getActions();
      expect(actions).toEqual([accountRequested(), accountRequestFailed(promiseRejectionError)]);
    });
  });

  describe('ACCOUNT_RECEIVED', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    afterEach(() => {
      jest.restoreAllMocks();
    });

    it('should set account expiry to localStorage and call setTimer if token expiry exists', () => {
      const localStorageSetItemSpy = jest.spyOn(Storage.prototype, 'setItem');
      const setTimerSpy = jest.spyOn(Timer.prototype, 'setTimer');
      store.dispatch(accountReceived(accountMock));
      expect(localStorageSetItemSpy).toBeCalledWith(localStorageKey, accountMock.exp);
      expect(setTimerSpy).toHaveBeenCalledTimes(1);
    });

    it('should not dispatch REFRESH_REQUESTED when token expiry is beyond the refreshIntervalMinutes', () => {
      const accountBeyondRefresh = accountMockWithCustomExpiry(refreshIntervalMinutes + 1);
      store.dispatch(accountReceived(accountBeyondRefresh));
      expect(refreshTokenRequest).not.toHaveBeenCalled();
      expect(store.getActions()).toEqual([accountReceived(accountBeyondRefresh)]);
    });

    it('should dispatch REFRESH_REQUESTED when token expiry is within the refreshIntervalMinutes', () => {
      const accountWithRefresh = accountMockWithCustomExpiry(refreshIntervalMinutes);
      store.dispatch(accountReceived(accountWithRefresh));
      expect(refreshTokenRequest).toHaveBeenCalledTimes(1);
      expect(store.getActions()).toEqual([accountReceived(accountWithRefresh), refreshRequested()]);
    });
  });

  describe('REFRESH_REQUESTED', () => {
    beforeEach(() => {
      localStorage.removeItem(localStorageKey);
      refreshTokenRequest.mockReturnValue(Promise.resolve());
    });
    it('should call refreshTokenRequest then dispatch REFRESH_RECEIVED when refreshToken Request succeed', async () => {
      await store.dispatch(refreshRequested());
      expect(refreshTokenRequest).toHaveBeenCalledTimes(1);
      const actions = store.getActions();
      expect(actions).toContainEqual(refreshReceived());
      expect(actions).toContainEqual(refreshRequested());
    });

    it('should call refreshTokenRequest then dispatch REFRESH_REQUEST_FAILED and SESSION_EXPIRED when refreshToken Request fail', async () => {
      refreshTokenRequest.mockRejectedValue(promiseRejectionError);
      await store.dispatch(refreshRequested());
      expect(refreshTokenRequest).toHaveBeenCalledTimes(1);
      const actions = await store.getActions();

      expect(actions).toEqual([refreshRequested(), refreshRequestFailed(promiseRejectionError), sessionExpired()]);
    });
  });

  describe('REFRESH_RECEIVED', () => {
    it('should dispatch ACCOUNT_REQUESTED', () => {
      store.dispatch(refreshReceived());

      const actions = store.getActions();
      expect(actions).toContainEqual(refreshReceived());
      expect(actions).toContainEqual(accountRequested());
    });
  });

  describe('REFRESH_REQUEST_FAILED', () => {
    beforeEach(() => {
      localStorage.removeItem(localStorageKey);
      jest.clearAllMocks();
    });

    afterEach(() => {
      jest.restoreAllMocks();
    });

    it('should dispatch SESSION_EXPIRED if token is not in local storage', () => {
      const localStorageGetItemSpy = jest.spyOn(Storage.prototype, 'getItem');
      const err = new Error();
      store.dispatch(refreshRequestFailed(err));

      const actions = store.getActions();
      expect(actions).toEqual([refreshRequestFailed(err), sessionExpired()]);
      expect(localStorageGetItemSpy).toHaveBeenCalledTimes(1);
      expect(deps.checkActive).toHaveBeenCalledTimes(0);
    });
  });

  describe('LOGOUT_REQUESTED', () => {
    it('should call logoutRequest then dispatch LOGOUT_RECEIVED when logout Request succeed', async () => {
      await store.dispatch(logOutRequested());
      expect(logoutRequest).toHaveBeenCalledTimes(1);
      const actions = store.getActions();
      expect(actions).toContainEqual(logOutRequested());
      expect(actions).toContainEqual(logOutReceived());
    });

    it('should call logoutRequest then dispatch LOGOUT_REQUEST_FAILED when logout Request fail', async () => {
      logoutRequest.mockRejectedValue(promiseRejectionError);
      await store.dispatch(logOutRequested());
      expect(logoutRequest).toHaveBeenCalledTimes(1);
      const actions = await store.getActions();
      expect(actions).toEqual([logOutRequested(), logOutRequestFailed(promiseRejectionError)]);
    });
  });

  describe('REFRESH_CANCELED', () => {
    it('should receive REFRESH_CANCELED and do nothing', () => {
      store.dispatch(refreshCancelled());
      expect(store.getActions()).toEqual([refreshCancelled()]);
    });
  });

  describe('other actions', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    afterEach(() => {
      jest.restoreAllMocks();
    });

    it('should call checkActive', () => {
      store.dispatch(action('foobar'));
      expect(deps.checkActive).toHaveBeenCalledTimes(1);
    });
  });
});
