import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';

export default {
  input: 'dist/lib/index.js',
  output: {
    file: 'dist/bundle.js',
    format: 'umd',
    name: 'redux-account-middleware',
  },
  plugins: [resolve(), commonjs()],
};
