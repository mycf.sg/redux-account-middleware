redux-account-middleware
============================
This library helps with the helps with the management of authentication account lifecycle

# Installation

```bash
npm i @govtechsg/redux-account-middleware --save
```
or
```bash
yarn add @govtechsg/redux-account-middleware
```

# Usage

To run the middleware, we'll have to connect it to the Redux Store.

#### `main.js`

```javascript
import {createStore, applyMiddleware, combineReducers} from 'redux'
import {
  accountReducer,
  createAccountMiddleware,
} from './middleware/account';

// create the account middleware
const accountMiddleware = createAccountMiddleware({
  fetchAccountRequest: fetch('getAccountRequest'),
  logoutRequest: fetch('logoutRequest'),
  refreshTokenRequest: fetch('refreshTokenRequest'),
});

import reducer from './reducers'

const sagaMiddleware = createSagaMiddleware()
// mount it on the Store
const store = createStore(
  // include the account reducer into the main reducer
  combineReducers({
    account: accountReducer,
    ...reducer,
  }),
  applyMiddleware(accountMiddleware)
)

// render the application
```

# Refresh token

This redux middleware will try to refresh the token by calling refresh token api if there is any dispatched action to the redux (given that user is logged in and token expiring within refreshIntervalMinutes)

there are exceptions where the middleware is in the middle of requesting new token and getting account info, or user is logging out  
which represented by status below

- `REFRESH_REQUESTED`: When refresh is already requested and in progress. This is to prevent duplicates refresh request.
- `REFRESH_RECEIVED`: This is the intermediate state where account api will be called in next action. This is to prevent duplicates refresh request.
- `ACCOUNT_REQUESTED`: This is the intermediate state to get the latest token expiry from account api. This is to prevent duplicates refresh request.
- `LOGOUT_REQUESTED`: When there is a logout request on-going, We should stop refreshing the token. This is so that the user session will not be re-populated with the fresh token and log the user back in

so when the state is either any one mentioned above, refresh token api will not be called